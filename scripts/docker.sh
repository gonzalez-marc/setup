#!/bin/sh

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

apt-key fingerprint 0EBFCD88

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
   
apt-get install docker-ce docker-ce-cli containerd.io -y

pip3 install docker-compose
systemctl enable docker
usermod -aG docker $SETUP_USER