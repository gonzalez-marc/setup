#!/bin/sh

addgroup $SETUP_SSH_GROUP
usermod -aG $SETUP_SSH_GROUP $SETUP_USER

sed -i "s/#Port 22/Port $SETUP_SSH_PORT/g" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin prohibit-password/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/#PubkeyAuthentication/PubkeyAuthentication/g" /etc/ssh/sshd_config
echo "AllowGroups $SETUP_SSH_GROUP" >> /etc/ssh/sshd_config

mkdir /home/$SETUP_USER/.ssh
touch /home/$SETUP_USER/.ssh/authorized_keys
for file in sshkeys/*
do
	cat $file >>> /home/$SETUP_USER/.ssh/authorized_keys
done
chmod 700 /home/$SETUP_USER/.ssh
chmod 600 /home/$SETUP_USER/.ssh/authorized_keys
chown -R $SETUP_USER:$SETUP_USER /home/$SETUP_USER