#!/bin/sh

apt-get install fail2ban -y

sed -i 's/action = %(action_)s/action = %(action_mwl)s/g' /etc/fail2ban/jail.conf
sed -i "s/destemail = root@localhost/destemail = $SETUP_MAIL_TO/g" /etc/fail2ban/jail.conf
sed -i "s/sender = root@<fq-hostname>/sender = $SETUP_MAIL_FROM/g" /etc/fail2ban/jail.conf
