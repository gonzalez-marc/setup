#!/bin/sh

# Config


USER=$(ls /home)
export SETUP_USER=$USER
export SETUP_SSH_GROUP=sshusers
export SETUP_SSH_PORT=2222
export SETUP_MAIL_TO=toto@toto.com
export SETUP_MAIL_FROM=server@toto.com

# Dependencies
apt-get update && apt-get upgrade \
        apt-listchanges \
        apt-transport-https \
        bash-completion \
        curl \
		gnupg2 \
        nano \
        openssh-server \
		python3-pip \
		mstmp \
		software-properties-common \
        sudo \
        unattended-upgrades -y
    

# Permissions
usermod -aG sudo $USER

# Scripts
sh ./scripts/ssh.sh
sh ./scripts/docker.sh
sh ./scripts/fail2ban.sh


